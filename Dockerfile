from waynectf:latest

EXPOSE 22
EXPOSE 80
EXPOSE 445

CMD ["/bin/bash","-c","smbd;apache2ctl start;/usr/sbin/sshd -D"]