# waynectf


## Running this image

### Docker on Linux

```
docker run -d -p 3000:22 -p 8080:80 -p 445:445 registry.gitlab.com/kcattakcaz/waynectf:latest
```

#### Docker on Windows

Docker on Windows will probably fail to start this container with `-p 445:445` because the port is already in use.  It must be mapped to another port on the host, in theory it should be possible to stop the Windows SMB service to free the port, but I could not figure this out.  However, using a different port may prevent you from accessing the share through Windows Explorer.

#### Podman (Fedora, Red Hat, etc.)

```
podman run -d -p 3000:22 -p 8080:80 -p 445:445 registry.gitlab.com/kcattakcaz/waynectf:latest
```


If you are using rootless podman (default setup in Fedora, and the reason I prefer it to Docker), you will probably see this error when running the container:

```
Error: rootlessport cannot expose privileged port 445...
```

You have two options to resolve this, you can change the `-p 445:445` to map to a higher port number on the host, which does not require root.  If you do this, you must configure your Samba client to use this port too.  The other option is to run:

```
sudo sysctl net.ipv4.ip_unprivileged_port_start=445
```
## Accessing running services

### SSH
SSH is running on port 3000 of your host if you used the command above.

`ssh root@localhost -p 3000`

The password is `password123`.

### Apache

Apache is running on port 8080 of your host:

Navigate to http://localhost:8080 and you should see the default apache2 welcome page

### Samba

Samba is running on port 445, to access it open your file browser like Nautilus (GNOME Files) and connect to smb://localhost/ you should see a folder shared as "sambashare".  In the container, the path is `/opt/sambashare`.

Username and password are the same as SSH, `root` and `password123`

## Create your own image or customize this one

Use [buildah](https://buildah.io/) to create images and [podman](https://podman.io/) to run them.

To create your own image, based on the Docker image for Ubuntu:

```
 container=$(buildah from ubuntu)
 buildah run $container bash

<You are in the container>

apt update

<Configure the system however you like, example below:>

apt install samba openssh-server apache2 vim curl

exit

<You are back to your host>

buildah commit $container your-new-image-name-here

```

If you want to customize _this specific image_ :

```
container=$(buildah from registry.gitlab.com/kcattakcaz/waynectf:latest)
buildah run $container bash

<Change whatever you want, as an easy example create a file within /opt/sambashare>

buildah commit $container your-new-image-name-here
```

You will need to modify the Dockerfile and change `FROM waynectf:latest` to your new image's name.

Technically, no Dockerfile is needed, the image can be configured with Buildah directly, but everyone expects to see a Dockerfile so here is a minimal one.